﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task07
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(numbers(5, 5, 5, 5, 5));
            Console.WriteLine(numbers(4, 4, 4, 4, 4));
            Console.WriteLine(numbers(3, 3, 3, 3, 3));
            Console.WriteLine(numbers(2, 2, 2, 2, 2));
            Console.WriteLine(numbers(1, 1, 1, 1, 1));
        }

        static int numbers(int a, int b, int c, int d, int e)
        {
            var output = a + b + c + d + e;
            return output;
        }
    }
}
